import pyautogui as pg
pg.FAILSAFE = True

class Holder:
    x = 0
    y = 0
    filled = 0
    count = 0
    def __init__(self, x, y):
        self.x = x
        self.y = y


    def checkfill(self):
        debug = True
        im = pg.screenshot()
        col = (29,31,28)
        os = 20
        if (25 < im.getpixel((self.x + os, self.y - os))[0] < 33):
            self.filled |= (1<<0)
        if (25 < im.getpixel((self.x + os, self.y))[0] < 33):
            self.filled |= (1<<1)
        if (25 < im.getpixel((self.x + os, self.y + os))[0] < 33):
            self.filled |= (1<<2)
        if (25 < im.getpixel((self.x - os, self.y + os))[0] < 33):
            self.filled |= (1<<3)
        if (25 < im.getpixel((self.x - os, self.y))[0] < 33 ):
            self.filled |= (1<<4)
        if (25 < im.getpixel((self.x - os, self.y - os))[0] < 33):
            self.filled |= (1<<5)
        self.count = bin(self.filled).count("1")
        return self.filled

    def getcount(self):
        return self.count

    def __gt__(self, other):
        return self.count > other.count

def checkpos(cx, cy):
    holders = []
    x, y = cx, cy

    pg.moveTo(x, y - 115)
    pg.moveTo(x + 100, y - 60)
    pg.moveTo(x + 100, y + 60)
    pg.moveTo(x, y + 115)
    pg.moveTo(x - 100, y + 60)
    pg.moveTo(x - 100, y - 60)

    holders.append(Holder(x, y - 115))
    holders.append(Holder(x + 100, y - 58))
    holders.append(Holder(x + 100, y + 58))
    holders.append(Holder(x, y + 115))
    holders.append(Holder(x - 100, y + 58))
    holders.append(Holder(x - 100, y - 58))
    return holders

def checkgameover(cx,cy):
    im = pg.screenshot()
    if(im.getpixel((cx, cy))) == (255,255,255):
        print("GAME OVER... retrying")
        pg.click(cx, cy)
        checkpos(cx, cy)

def checkValid(c, h):
    print (bin(c.checkfill()))
    print (bin(h.checkfill()))
    print(" ")
    return (c.checkfill() & h.checkfill()) == 0

def main():
    print("please hold mouse in center")
    input("press enter...")
    cx, cy = pg.position()
    holders = checkpos(cx, cy)
    for i in holders:
        i.checkfill()

    for runs in range(50):
        print("RUN ", runs)
        centerh = Holder(cx, cy)
        validMoves = []

        for i in holders:
            if (checkValid(centerh, i)):
                validMoves.append(i)
        if len(validMoves) == 0:
            pg.click(cx,cy)
        else:
            maxfilled = max(validMoves)
            pg.click(maxfilled.x, maxfilled.y)
        checkgameover(cx,cy)

main()


